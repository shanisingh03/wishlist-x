<?php

namespace App\Http\Controllers;

use Auth;
use App\Setting;
use Illuminate\Http\Request;


class SettingsController extends Controller
{
    /**
     * create Snippet Code
     * @param Nill
     */
    public function getSettings()
    {
        return view('settings');
    }

    /**
     * Configure Theme 
     * @for Used To Configure Your Theme
     * @param Request $_REQUEST
     * @return
     * @author Shani Singh
     */
    public function configureTheme(Request $request)
    {
       // Get Current Shop
       $shop = Auth::user();
        
       // Get Themes of Shop
       $themes = $shop->api()->rest('GET','/admin/themes.json');

       // Active Theme ID
       $activeThemeId = "";
       foreach ($themes['body']->container['themes'] as $theme) {
           # Theme Id With Role Main
           if($theme['role'] == "main"){
               $activeThemeId = $theme['id'];
           }
       }

       // Snippet Code
       $snippet = "This Snippet code is Written By Axios ";

       // Data To pass to API Request
       $snippetData = array(
           "asset" => array(
               "key" => "snippets/sentriqo-wishlist-x-app.liquid",
               "value" => $snippet
           )
       );

       // Send Request to Shopify Theme    
       $shop->api()->rest('PUT','/admin/themes/'.$activeThemeId.'/assets.json',$snippetData);

        // Save Data In DB
        Setting::updateOrCreate(
            ['shop_id' => $shop->name],
            ['activated' => true]
        );
        
       return ["message" => "Theme Setup Successfully."];
    }
}
