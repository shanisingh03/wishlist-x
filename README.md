## Wishlist-X Shopify App

The wishlist-X app is made for shopify store, Which help to make wishlist work in shopify.

## Author

Shani Singh (CEO & MD) [Sentriqo IT Solutions Pvt Ltd](https://sentriqoitsolutions.com)


## License

The WishList-X App is software licensed under the [MIT license](https://opensource.org/licenses/MIT).
