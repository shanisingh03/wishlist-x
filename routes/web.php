<?php

use Auth;
use App\Setting;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $setting = Setting::where('shop_id',Auth::user()->name)->first();

    return view('dashboard')->with('setting',$setting);

})->middleware(['auth.shopify'])->name('home');


Route::middleware(['auth.shopify'])->group(function() {
    // Products
    Route::view('/products','products');

    // Customers
    Route::view('/customers','customers');

    // Settings
    Route::get('/settings','SettingsController@getSettings');
    Route::post('/configure','SettingsController@configureTheme');
});


